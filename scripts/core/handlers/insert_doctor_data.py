import json
from scripts.config.configuration import r

doctor_id = int(input("Enter doctor id >> "))


def get_input():
    while True:
        if r.exists(doctor_id):
            print("Doctor ID exists already!")
            break
        else:
            doctor_name = input("enter doctor_name >> ")
            doctor_age = int(input("enter doctor_age >> "))
            doctor_data = {
                'doctor_id': doctor_id,
                'doctor_name': doctor_name,
                'doctor_age': doctor_age
            }
            print(doctor_data)
            doc = r.set(doctor_id, json.dumps(doctor_data))
            print("Inserted data >> ", doc)
            break


def read_doc_data():
    doc_dat = r.get(doctor_id)
    print("doctor data >> ", doc_dat)


# def delete_doc():
#     doct = int(input("enter doc id>> "))
#     del_doc = r.delete(doct, doctor_id)
#     print("deleted", del_doc)


if __name__ == '__main__':
    get_input()
    read_doc_data()
