import json
#
# from scripts.constants.database import red, r
from scripts.config.configuration import red, r
from scripts.core.handlers.sample import read_doc_data

# from scripts.core.handlers.

patient_id = int(input("enter patient_id to verify whether the patient id exists or to create "
                       "new id to view the record >> "))
fin_doc = 0
d3 = 0


def get_input():
    global fin_doc, d3
    while True:
        if r.exists(patient_id):
            # fin_docc = r.get(fin_doc) - patient_id
            fin
            print("your doc >> ", fin_docc)
            print("You are already registered with the doctor!")
            break
        else:
            patient_name = input("enter patient_name >> ")
            patient_age = int(input("enter patient_age >> "))
            patient_description = input("enter description >> ")
            patient_data = {
                'patient_id': patient_id,
                'patient_name': patient_name,
                'patient_age': patient_age,
                'patient_description': patient_description
            }
            # pat = r.set(patient_id, json.dumps(patient_data))
            pat1 = red.set(patient_id, json.dumps(patient_data))
            print("patient details >> ", type(patient_data))
            print("Patient details >> ", pat1)
            doct_data = read_doc_data()
            print(doct_data)
            d1 = patient_data['patient_id']
            print("patient_data['patient_id'] >> ", d1, type(d1))

            res_dict = json.loads(doct_data.decode('utf-8'))

            # printing type and dict
            print("The type after conversion to dict is : " + str(type(res_dict)))
            print("The value after conversion to dict is : " + str(res_dict))
            d2 = res_dict['doctor_id']
            print("doctor_data['patient_id'] >> ", d2, type(d2))
            if d1 != d2:
                fin_doc = patient_data['patient_id'] + res_dict['doctor_id']
                print("final value >> ", fin_doc)
                d3 = r.set(fin_doc, json.dumps(res_dict))
                print("doctor id is > ", d3)
            break


def show_data():
    showing_data = r.get(patient_id)
    print("Data from the database >> ", showing_data)


if __name__ == '__main__':
    choose = int(input("Choose: \n1) To insert data\n2) To read data from the database\n"
                       "3) Try again >> \n"))
    if choose == 1:
        get_input()
    elif choose == 2:
        show_data()
