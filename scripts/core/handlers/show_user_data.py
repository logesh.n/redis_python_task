import redis
from scripts.core.handlers.user_data import patient_id
from scripts.config.configuration import r


def show_records():
    show = r.get(patient_id)
    print("DB records >> ", show)

#
# if __name__ == '__main__':
#     show_records()
